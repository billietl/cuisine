Title: Tajine au poulet, olives, amandes et abricots secs
Date: 2010-12-03 10:20
Category: Plat
Inspiration: Compilation de diverses recettes
Tags: Plat,Exotique
Preparation: 30 min
Cuisson: 1 h 30 min
Prix: €€
Difficulte: Facile

# Ingrédients

- 4 blancs de poulet
- 1 courgette
- 2 poivrons verts
- 200 g olives vertes dénoyautées
- 150 g amandes émondées
- 250 g abricots secs
- 50 g gingembre
- 2 gousses d'ail
- ras el-hanout
- harissa
- huile d'olive

# Instructions

Pré-chauffez le four à 180 °C.

Coupez le poulet en morceaux.
Mélangez dans un cul de poule 1 cuiller à soupe de ras el-hanout, 2 cuillers à soupe d'huile et le poulet.
Laissez mariner 30 minutes.

Détaillez les poivrons et les courgettes.
Réservez.
Hachez l'ail et le gingembre.
Réservez.

Dans un fait-tout allant au four, faites suer l'ail et le gingembre.
Ajoutez-y le poulet et faites-le dorer.
Ajoutez dans l'ordre les légumes, les amandes, les olives et les abricots.
Ajoutez 2 cuillers à soupe de ras el-hanout, de la harissa selon le goût et de l'eau à mi-hauteur.

Enfournez 1 heure.
