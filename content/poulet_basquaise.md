Title: Poulet basquaise
Date: 2010-12-03 10:20
Category: Plat
Tags: Plat,Soleil,Été
Preparation: 15 min
Cuisson: 30 min
Prix: €
Difficulte: Facile

# Ingrédients

- 2 cuisses de poulet
- 4 oignons
- 6 poivrons
- 500 g de coulis de tomates
- 25 cl de vin blanc
- 3 gousses d'ail
- sel, poivre, thym, romarin, piment
- huile d'olive

# Instructions

Épluchez et émincez les oignons et l'ail.

Dans un fait-tout, faites revenir les oignons dans de l'huile d'olive.

Une fois les oignons cuits, ajoutez l'ail, les poivrons, le poulet et le vin.
Laissez mijoter jusqu'au premier bouillon.

Ajoutez le coulis de tomate et les aromates.

Laissez mijoter 20 minutes à feu doux.

Vous pouvez accompagner ce plat avec du riz.
