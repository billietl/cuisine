Title: Kisir
Date: 2010-12-03 10:20
Category: Plat
Tags: Végétarien
Preparation: 20 min
Repos: 10 min
Prix: €
Difficulte: 1
Photo: false

# Ingrédients

- 350 g semoule de blé
- 530 ml eau
- 1 botte de cébettes
- 1 botte de persil
- 7 branches de menthe
- 2 cuillers à soupe de double concentré de tomates
- 1 cuiller à soupe de pâte de piment
- 1 cuiller à café de cumin
- 7 cuillers à soupe d'huile d'olive
- 5 cuillers à soupe de mélasse de grenade
- citron
- sel
- poivre

# Instructions

Faites bouillir la moitié de l'eau. Mélangez la semoule avec toute l'eau (il devrait y avoir le même volume d'eau et de semoule) et couvrez. Laissez reposer jusqu'à ce que la semoule ait complètement refroidi.

Ciselez les cébettes, le persil et la menthe. Réservez.

Une fois que la semoule est à température ambiante, mélangez-y le concentré de tomates, la pâte de piment, le sel, le poivre (), le cumin et l'huile d'olive. Le mélange doit être bien humide sans rejeter trop de liquide en le pressant.

Ajoutez les cébettes, le persil, la menthe et la mélasse de grenade, mélangez sans presser.

Servez avec des quartiers de citron pour en ajouter le jus selon le goût.
