Title: Crostate
Date: 2010-12-03 10:20
Category: Pâtisserie
Tags: Dessert,Calories,Exotique
Inspiration: Recette familiale
Preparation: 35 min
Cuisson: 45 min
Prix: €
Difficulte: Facile
Photo: false

# Ingrédients

- 600 g de farine
- 250 g de sucre
- 1 sachet de levure
- 2 œufs
- 1 verre d'huile
- 1 verre de lait
- 300 g de confiture

# Instructions

Mélangez dans un saladier la farine, la levure et le sucre.
Ajoutez les œufs et l'huile et mélangez.
Incorporez le lait petit à petit jusqu'à obtention d'une pâte à tarte.

Filmez la pâte et laissez là reposer 20 minutes au réfrigérateur.

Utilisez 2/3 de la pâte pour foncer le moule.
Garnissez avec la confiture.
Utilisez la pâte restante pour faire un quadrillage par-dessus.

Enfournez à froid, allumez le four à 140 °C et laissez cuire jusqu'à ce que la pâte soit légèrement brunie.
