Title: Nutelouis
Date: 2010-12-03 10:20
Category: Patisserie
Tags: Patisserie,Calories,Condiment
Preparation: 20 min
Cuisson: 30 min
Prix: €€
Difficulte: Facile
Photo: false

# Ingrédients

- 500 g de noisettes émondées
- 70 g de sucre glace
- 400 g de chocolat noir
- 1 pincée de lentilles vertes
- 240 g sucre blanc
- huile de tournesol

# Instructions

Faites chauffer le four à 150 °C.

Dans une poêle, faites chauffer le sucre blanc pour réaliser un caramel à sec.
Débarrasser sur une plaque avec un papier cuisson.
Laisser refroidir complètement.

Sur un autre plaque à four recouverte de papier cuisson, étalez les noisettes en une seule couche.
Enfournez 25 à 30 minutes à 150 °C.
Sortez les noisettes du four et laissez refroidir.

Frottez les noisettes dans un chiffon propre pour retirer les peaux.

Dans un robot mixeur, mettez la moitié des noisettes et le caramel cassé en morceaux.
Mixez jusqu'à obtention d'une pâte liquide.
Réservez.

Faites fondre le chocolat au bain marie.

Dans le robot mixeur, mixez le restant des noisettes jusqu'à obtention d'une pâte liquide.
Ajoutez le chocolat, le mélange noisettes-caramel, le sel et un peu d'huile.
Débarrassez dans des bocaux propres.
