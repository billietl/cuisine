Title: Gâteau nantais
Date: 2010-12-03 10:20
Category: Pâtisserie
Tags: Dessert,Calories,Beurre
Inspiration: https://www.levoyageanantes.fr/a-voir/voyage-culinaire/gateau-nantais/
Preparation: 15 min
Cuisson: 45 min
Prix: €
Difficulte: Facile
Photo: false

# Ingrédients

- 150 g de sucre en poudre
- 125 g de beurre demi-sel
- 100 g d’amandes en poudre
- 40 g de farine
- 3 œufs
- 6 cl de lambig (ou autre alcool fort)
- 50 g de sucre glace

# Instructions

Faites préchauffer le four à 180 °C.

Fouettez énergiquement le beurre pommade avec le sucre.

Ajoutez les amandes en poudre, puis les oeufs un a un.
Bien mélanger jusqu'à ce que le sucre soit fondu.

Ajouter à la spatule 2 cl de lambig puis la farine.

Beurrez et farinez le moule, versez-y la pâte et enfournez 45 minutes.

Démoulez le gâteau et imbibez-le de 2 cl de lambig par le dessous tant qu'il est chaud.

Mélangez le reste du lambig et le sucre glace pour préparer le glaçage et glacez le gâteau une fois qu'il est refroidi.
