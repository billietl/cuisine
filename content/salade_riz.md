Title: Salade de riz grecque
Date: 2010-12-03 10:20
Category: Plat
Inspiration: https://www.mushfood.fr/recipe/details/salade-de-riz-grecque
Tags: Salade,Été,Grèce
Preparation: 10 min
Cuisson: 20 min
Prix: €€
Difficulte: Facile

# Ingrédients

- 240 g de riz basmati
- 150 g de pistaches
- 100 g de crevettes grises
- 100 g d'emmental
- 50 g d'olives noires
- 4 cuillers à soupe d'huile d'olive
- 2 cuillers à soupe de jus de citron
- 2 cuillers à café de sauce worcestershire
- 1 cuiller à café de moutarde
- sel, poivre

# Instructions

Cuire le riz à grande eau. Égouttez et rincez à l'eau froide.

Mélangez dans un saladier l'huile, le jus de citron, la sauce worcestershire, la moutarde, le sel et le poivre.

Décortiquez les crevettes, émondez les pistaches, dénoyautez les olives, découpez l'emmental en dés.

Assemblez le tout dans le saladier.
