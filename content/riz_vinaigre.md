Title: Riz vinaigré
Date: 2010-12-03 10:20
Category: Plat
Tags: Accompagnement,Japon,Exotique
Preparation: 30 min
Cuisson: 30 min
Prix: €€
Difficulte: Facile

# Ingrédients

- 150 g de riz à sushi (ou riz rond)
- 225 g d'eau
- 3 cuillers à soupe de vinaigre de riz
- 3 cuillers à café de sucre
- 1 cuiller à café de sel

# Instructions

Lavez le riz, réservez.

Mélangez dans un bol le vinaigre, le sucre et le sel.

Mettez dans une casserole le riz et l'eau.
Faites chauffer couvert et à feu moyen jusqu'à ébullition.
Laissez cuire 5 minutes à feu moyen, puis 10 minutes à feu doux, puis 10 minutes hors du feu.

Transvasez le riz dans un cul de poule et ajoutez-y le mélange de vinaigre.
Mélangez délicatement.

Laissez refroidir à température ambiante en mélangeant délicatement régulièrement.
Vous pouvez vous servir d'un éventail pour accélérer le refroidissement.
