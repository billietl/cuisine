Title: Ramen au miso
Date: 2010-12-03 10:20
Category: Plat
Inspiration: Création originale
Tags: Végétarien,Japon,Exotique
Preparation: 30 min
Cuisson: 30 min
Prix: €€
Difficulte: Facile

# Ingrédients

- 35 cl de bouillon de légume
- 3 cuillers à soupe de sauce soja salée
- 1 cuiller à soupe bombée de miso blanc
- 1 cuiller à café d'huile de sésame
- 1 cuiller à café de vinaigre de riz
- 1 cuiller à café rase de pâte de piment
- 1 faisceau de nouilles somen
- 1 feuille de nori
- 1 œuf

# Instructions

Faite cuire l'œuf mollet.
Écallez-le, réservez.

Mettez dans une casserole le bouillon de légumes, le miso, la sauce soja, le vinaigre de riz, l'huile de sésame et la pâte de piment.
Faites chauffer sans que ça n'entre en ébullition.

Faites cuire les nouilles à l'eau claire sans sel.
Égouttez, réservez.

Dans un grand bol, versez le bouillon.
Placez-y les nouilles, les œufs coupés en deux sur les nouilles, la feuille de nori coupée en deux sur le bord.
