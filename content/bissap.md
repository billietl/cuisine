Title: Bissap
Date: 2010-12-03 10:20
Category: Boisson
Inspiration: https://www.papillesetpupilles.fr/2021/12/comment-faire-du-bissap-la-boisson-aux-fleurs-dhibiscus.html/
Tags: Boisson,Exotique,Végétarien
Cuisson: 20 min
Prix: €
Difficulte: Facile
Photo: false

# Ingrédients

- 15 g fleur d'hibiscus rouge
- 100 g sucre
- 1 cuiller à café de fleur d'oranger
- 1 cuiller à soupe de mélasse de grenade

# Instructions

Faites bouillir 1 litre d'eau.
Retirez la casserole du feu et mettez-y tous les ingrédients.
Laissez infuser 15 min en remuant régulièrement.

Filtrez les fleurs d'hibiscus.
Elles peuvent être réutilisées une seconde fois.

À consommer tiède ou glacé.
