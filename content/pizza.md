Title: Pizza
Date: 2010-12-03 10:20
Category: Plat
Inspiration: https://www.youtube.com/watch?v=zSK6NaaUxZ8
Tags: pâtisserie,Italie
Preparation: 48 h
Cuisson: 10 min
Prix: €
Difficulte: Facile

# Ingrédients

- 150 g de farine 
- 87 g d'eau de source
- 1 cuiller à café de levure sèche
- 3 g de sel
- huile d'olive

# Instructions

Dans un saladier, mélangez la farine, la levure et l'eau sans écraser la pâte.

Ajoutez le sel à la pâte et incorporez en pétrissant.

Ajoutez un filet d'huile d'olive et incorporez en pétrissant.

Pétrissez la pâte jusqu'à ce qu'elle soit lisse.

Faites lever la pâte couvert d'un linge propre humide.

Réservez au frigo 48 h.

Préchauffez votre four à 250°C.

Étalez la pâte sur un plan de travail fleuré.

Ajoutez la garniture de votre choix. Attention à ne pas trop mouiller la pâte avec la sauce tomate, ou à trop couvrir de garniture.

Faites cuire au four sur une plaque perforée 10 min.
