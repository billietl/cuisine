Title: Délicieuse salade d'endives
Date: 2010-12-03 10:20
Category: Plat
Inspiration: La cuisine d'Alexia (éditions Marabout)
Tags: Plat,Salade,Végétarien
Preparation: 15 min
Prix: €
Difficulte: facile
Photo: true

# Ingrédients

- 8 endives
- 4 poire conférence
- 4 poignées de noisettes
- 120 g de bleu d'auvergne
- 12 cuiller à soupe d'huile d'olive
- 6 cuiller à soupe de vinaigre balsamique
- 4 cuiller à café de moutarde
- 4 cuiller à café de miel
- sel, poivre

# Instructions

Dans un saladier, mélanger la moutarde, le miel, le sel et le poivre. Ajoutez le vinaigre et l'huile.

Coupez les endives en tronçon et ôtez le coeur. Coupez les poires et le fromage en dés. Concassez les noisettes.

Mélangez le tout.
