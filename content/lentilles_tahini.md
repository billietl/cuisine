Title: Lentilles au tahini
Date: 2010-12-03 10:20
Category: Plat
Tags: Exotique,Végétarien
Inspiration: Plenty more (éditions Hachette cuisine)
Preparation: 10 min
Cuisson: 20 min
Prix: €
Difficulte: Facile

# Ingrédients

- 200 g de lentilles vertes
- 6 cuillers à soupe de tahini
- 1 petit oignon rouge
- 1 bouquet de coriandre
- 2 citrons
- 4 oeufs
- 20 cL de purée de tomates
- 4 gousses d'ail
- 1 cuiller à soupe de cumin
- beurre
- huile d'olive
- paprika fumé, pimenton fumé ou piment chipotle

# Instructions

Faites cuire les lentilles à l'eau.
Réservez.

Cuisez les oeufs pour qu'ils soient durs.
Ecallez-les, et réservez.

Dans un bol, mélangez le tahini, le jus des citrons et un peu d'eau pour avoir la consistance d'une sauce fluide.
Réservez.

Epluchez et coupez l'ail en rondelles.
Epluchez et coupez l'oignon en lamelles.
Lavez et hachez la coriandre.
Réservez le tout.

Dans une sauteuse, faites fondre le beurre avec de l'huile d'olive.
Ajoutez l'ail et le cumin et faites cuire à feu doux jusqu'à ce que l'ail commence à prendre des couleurs.
Ajoutez la purée de tomates, les lentilles et le mélange de tahini.
Mélangez bien et laissez mijoter 5 minutes.
Ajoutez les oignons et la coriandre, mélangez et faites mijoter 5 minutes.

Servez dans une assiette creuse avec un peu de paprika fumé, de pimenton fumé ou de piment chipotle en fonction du goût et un filet d'huile d'olive.
