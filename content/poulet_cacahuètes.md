Title: Poulet sauce cacahuètes
Date: 2010-12-03 10:20
Category: Plat
Inspiration: Recette familiale
Tags: Plat
Preparation: 10 min
Cuisson: 20 min
Repos: 1 nuit
Prix: €
Difficulte: Moyen

# Ingrédients

- 4 cuisses de poulet
- 1 citron
- 2 cuillers à soupe de sauce soja
- 1 cuiller à soupe d'huile
- 3 gousses d'ail
- 1 oignon
- 6 cuillers à café de sucre
- 40 cl d'eau
- 2 bouillon-cubes de poule
- 400 ml de lait de coco
- 250 g de beurre de cacahuètes
- 1/2 cuiller à café de purée de piment
- 1 yaourt nature

# Instructions

Préparez la marinade avec le jus du citron, la sauce soja, l'huile, l'ail écrasé, l'oignon haché, 2 cuillers à café de sucre.
Mettez à mariner le poulet une nuit.

Dans une casserole, dissolvez le bouillon cube dans l'eau chaude.

Ajoutez le beurre de cacahuètes et le lait de coco.

Ajoutez 4 cuillers à café de sucre et le piment.

Faites cuire le poulet à la poêle.

En fin de cuisson, ajoutez le yaourt.
