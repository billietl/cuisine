Title: Paupiettes sauce moutarde
Date: 2010-12-03 10:20
Category: Plat
Inspiration: Recette familiale
Tags: Plat
Preparation: 10 min
Cuisson: 20 min
Prix: €
Difficulte: Moyen

# Ingrédients

- 4 paupiettes
- 2 oignons
- 50 cl de crème fraiche épaisse
- 1 pot de moutarde forte
- beurre
- thym, laurier

# Instructions

Couvrez les paupiettes d'une couche de moutarde.

Dans un fait-tout, faite revenir les paupiettes dans du beurre.
Réservez les paupiettes.
Faites revenir les oignons dans le fait-tout.

Ajoutez la crème, la moutarde, et mélangez.
Ajoutez les paupiettes, le thym et le laurier.

Laissez mijoter à feu moyen 10 minutes en remuant régulièrement.

Vous pouvez servir ce plat avec du blé et des haricots verts.
