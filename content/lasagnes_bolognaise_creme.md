Title: Lasagnes à la bolognaise et à la crème
Date: 2010-12-03 10:20
Category: Plat
Inspiration: recette familiale
Tags: Tradition,calories
Preparation: 10 min
Cuisson: 1 h
Prix: €€
Difficulte: Moyen

# Ingrédients

- 500 g pâtes sèches pour lasagnes
- 300 g bœuf haché
- 300 g chair à saucisse
- 1,2 kg pulpe de tomates
- 3 gousses d'ail
- 50 cl de crème fraîche liquide
- 3 boules de mozzarella
- 200 g de gruyère râpé
- origan, basilic, thym, laurier, romarin
- huile d'olive
- sel, poivre

# Instructions

Dans un fait-tout, faites cuire à feu vif la viande de bœuf, la chair à saucisse et l'ail émincé.

Une fois que la viande est complètement cuite, ajoutez la pulpe de tomates et les aromates (4 d'origan, 4 de basilic, 2 de thym, 1 de romarin, 2 feuilles de laurier) et laissez mijoter à feu doux pendant 20 minutes.

Préchauffez le four à 200 °C.

Découpez la mozzarella en rondelles.

Dans un plat à gratin, couvrez le fond d'une fine couche de bolognaise, puis mettez une couche de pâte, une couche de bolognaise, puis une couche de pâte.

Ajoutez quelques rondelles de mozzarella et couvrez l'étage de crème liquide.

Mettez une couche de pâte, puis de bolognaise, puis de pâte.

Pour le dernier étage, mettez une couche de bolognaise, le reste de mozzarella et de crème et finissez avec le gruyère.

Faites cuire 20 à 30 minutes au four.
