Title: Houmous
Date: 2010-12-03 10:20
Category: Plat
Tags: Végétarien
Preparation: 10 min
Repos: 2 h
Prix: €
Difficulte: 1
Photo: false

# Ingrédients

- 1 grosse boite de pois chiches (400 g égoutté)
- 2 gousses d'ail
- 6 cuillers à soupe de tahini
- 1 citron
- 2 cuillers à soupe d'huile d'olive
- glaçons
- 1/2 cuiller à café de sel

# Instructions

Mettez dans un robot mixeur les pois chiches, l'ail, le tahini, l'huile d'olive, le sel et le jus du citron.

Mixez à basse vitesse pendant 7 à 8 minutes en ajoutant un glaçon chaque minute. Goûtez et ajustez l'assaisonnement en sel, jus de citron et tahini.

Mixez à haute vitesse quelques instants afin d'avoir une texture crémeuse. Laissez reposer au frais 2 heures pour un résultat optimal.
