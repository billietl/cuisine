Title: Vin chaud
Date: 2010-12-03 10:20
Category: Boisson
Tags: Boisson,Alcool,Fêtes
Preparation: 15 min
Cuisson: 1 h
Prix: €€
Difficulte: Facile
Photo: false

# Ingrédients

- 1 orange
- 1 citron
- 1 bâton de cannelle
- 1 étoile de badiane
- 2 cardamomes
- 3 clous de girofle
- 100 g sucre de canne
- 75 cl de vin rouge Syrah

# Instructions

Dans un fait-tout, placez l'écorce des agrumes, les épices, le sucre et le vin.
Laissez reposer 30 minutes.

Faites chauffer à feu moyen 30 minutes, le vin ne doit pas bouillir.
Mélangez régulièrement.

Filtrez avant de servir.

