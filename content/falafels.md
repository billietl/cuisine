Title: Falafels
Date: 2010-12-03 10:20
Category: Plat
Inspiration: https://www.mesinspirationsculinaires.com/article-recette-falafel-facile.html
Tags: Plat,Végétarien,Végétalien,Exotique
Preparation: 1 h 20 min
Cuisson: 10 min
Prix: €€
Difficulte: Facile

# Ingrédients

- 200 g de pois chiche secs trempées la veille
- 2 gousses d'ail
- 1 quart d'oignon
- 1 demi cuiller à café de bicarbonate
- 1 demi cuiller à soupe de graines de sésames dorés
- 1 demi bouquet de persil (ou coriandre ou menthe, ou mélange des deux)
- 1 cuiller à café cumin
- 1 cuiller à café coriandre en poudre
- sel, poivre

# Instructions

Égouttez, rincez et séchez les pois chiches à l'aide d'un torchon propre.

Hachez grossièrement l'oignon et l'ail.

Mixez tous les ingrédients ensemble. Procédez par impulsion, le résultat doit être une pâte grossière entre le couscous et le houmous.

Débarassez la pâte, filmez-la et réservez au frigo 1h.

Formez des petites boules et faites cuire à l'huile.
