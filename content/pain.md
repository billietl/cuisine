Title: Pain
Date: 2010-12-03 10:20
Category: Boulangerie
Inspiration: Recette de Lauren Chemla
Tags: Boulangerie,Pain
Preparation: 1 h 30 min
Repos: 1 h
Cuisson: 15 min
Prix: €
Difficulte: Facile

# Ingrédients

- 500 g de farine T65
- 7 g de levure sèche
- 10 g de sel
- 25 g de sucre
- 200 g d'eau de source
- 75 g de lait demi-écrémé
- 30 g de beurre

# Instructions

Pétrissez les ingrédients dans une machine à pain sur le programme "pâte".

Formez des petites boules, laissez lever 5 minutes, applatissez-les.

Laissez lever 1 h à temperature ambiante.

Faites cuire 15 min à 200°C.
