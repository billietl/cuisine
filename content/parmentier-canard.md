Title: Parmentier de canard au potimarron
Date: 2010-12-03 10:20
Category: Plat
Tags: Plat
Preparation: 1 h 30 min
Cuisson: 20 min
Prix: €
Difficulte: Facile

# Ingrédients

- 1 potimarron
- 4 cuisses de canard confit
- 100 g de noisettes

# Instructions

Épluchez et détaillez le potimarron. Faites-le cuire à grande eau.

Torréfiez les noisettes dans une poêle anti-adhésive. Retirez la peau après cuisson.

Faites cuire les confits de canard à feu doux en retirant régulièrement l'excès de graisse.

Faites préchauffer le four à 180 °C.

Dépiautez les confits de canard. Effilochez-les à l'aide d'une fourchette.

Réduisez le potimarron en purée. Assouplissez-le à l'aide d'un peu d'eau de cuisson (la purée obtenue doit être consistante).

Mélangez les noisettes à la purée.

Dans un plat à gratin, disposez l'effiloché de canard, puis la purée de potimarron.

Enfournez 20 minutes.
