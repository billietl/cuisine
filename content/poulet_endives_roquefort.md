Title: Poulet aux endives et à la crème de roquefort
Date: 2010-12-03 10:20
Category: Plat
Inspiration: Recette familiale
Tags: Plat,fromage
Preparation: 10 min
Cuisson: 45 min
Prix: €
Difficulte: Facile

# Ingrédients

- 1 Kg d'endives
- 5 échalottes
- 4 cuisses de poulet
- 25 cl de vin blanc
- 1 pointe de roquefort
- 25 cl de crème épaisse
- beurre

# Instructions

Coupez les endives en tronçons.
Faites-les cuire dans un grand volume d'eau salée 15 minutes.
Égouttez-les et pressez-les pour en extraire le jus.
Réservez.

Épluchez les échalottes et émincez-les.

Dans un fait-tout, faites blondir les échalottes dans du beurre.

Ajoutez les cuisses de poulet et le vin.
Couvrez et laissez cuire 10 minutes environ.

Une fois que le poulet est cuit, ajoutez les endives, puis la crème, puis le roquefort.
Laissez mijoter à feu doux sans agiter 10 minutes environ.

Une fois que le roquefort est fondu, mélangez et laissez mijoter encore 5 minutes.
