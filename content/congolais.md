Title: Congolais
Date: 2010-12-03 10:20
Category: Biscuits
Inspiration: Gâteaux de mamie (éditions Marabout)
Tags: Biscuit,Sucre
Preparation: 20 min
Cuisson: 10 min
Prix: €€
Difficulte: Facile
Photo: false

# Ingrédients

- 200 g de noix de coco râpé
- 130 g de sucre
- 3 œufs
- 2 cuillers à café de cannelle moulue
- 20 g de sucre vanillé

# Instructions

Mélangez dans un saladier tous les ingrédients.
Faites attention à ne pas faire mousser les œufs.

Laissez reposer le mélange 30 minutes.

Faites préchauffer le four à 180 °C.

Formez des rochers sur une plaque de cuisson.

Enfournez 10 minutes.
